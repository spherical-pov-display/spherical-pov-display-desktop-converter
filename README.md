# Spherical POV Display Desktop Converter

## EXPERIMENTAL
This is early POC code for the animation extention.

## General
The Desktop Converter converts an image sequence of a video into an anim file that can be played on the Spherical POV Display.
The images are currently located in the /amin01 folder.

## Requirements
The Host script requires the Python Image Library Pillow (https://python-pillow.org/).