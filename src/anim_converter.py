from PIL import Image
import time
import os
import json
import base64

HORIZONTAL_RES = 100
VERTICAL_RES = 112
CHANNELS_PER_LED = 4

images = []
images_enc = []

gamma_lut = [    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255]

def map_image(image):
	def vertical_mapping(vertical_position):
		if vertical_position >= 0 and vertical_position < VERTICAL_RES:
			if(vertical_position % 2 == 0):
				return (VERTICAL_RES - 1 - vertical_position) / 2
			else:
				return (VERTICAL_RES + vertical_position) / 2

	def horizontal_mapping(horizontal_position, vertical_position):
		if horizontal_position >= 0 and horizontal_position < HORIZONTAL_RES and vertical_position >= 0 and vertical_position < VERTICAL_RES:
			if vertical_position % 2 == 0:
				return horizontal_position
			else:
				return (horizontal_position + HORIZONTAL_RES / 2) % HORIZONTAL_RES

	mapped_image = Image.new("RGB", image.size)
	width, height = image.size

	pix_image = image.load()
	pix_mapped = mapped_image.load()

	for i in range(0, width):
		for j in range(0, height):
			h_pos = horizontal_mapping(i, j)
			v_pos = vertical_mapping(j)
			bgr = (pix_image[i, j][2], pix_image[i, j][1], pix_image[i, j][0])
			pix_mapped[h_pos, v_pos] = bgr

	return pix_mapped

def get_buffer_from_image(image):
	print("Loading image: " + image.filename)
	buffer = b''
	width, height = image.size
	if width == HORIZONTAL_RES and height == VERTICAL_RES:
		start = time.time()
		pix_map = map_image(image)
		for i in range(0, width):
			for j in range(0, height):
				buffer += bytes([31])
				buffer += bytes([gamma_lut[pix_map[i, j][0]]])
				buffer += bytes([gamma_lut[pix_map[i, j][1]]])
				buffer += bytes([gamma_lut[pix_map[i, j][2]]])
		end = time.time()
		print("Took: " + str((end - start) * 1000) + "ms")
		return buffer
	else:
		return -1

def load_anim_images():
	images.clear()
	file_list = os.listdir("anim01")
	file_list.sort()
	
	for file in file_list:
		if file.endswith("png") or file.endswith("jpg") or file.endswith("bmp"):
			images.append(get_buffer_from_image(Image.open(os.path.join("anim01", file))))
	print("Loading images DONE")

load_anim_images()

for image in images:
    # https://stackoverflow.com/questions/40000495/how-to-encode-bytes-in-json-json-dumps-throwing-a-typeerror
    encoded = base64.b64encode(image)
    images_enc.append(encoded.decode('ascii'))

with open('world.anim', 'w') as file:
    file.write(json.dumps(images_enc))